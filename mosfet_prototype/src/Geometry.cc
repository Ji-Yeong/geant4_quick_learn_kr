//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Geometry.cc
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "Geometry.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include "G4NistManager.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"
#include "SensitiveVolume.hh"
#include "G4SDManager.hh"
//------------------------------------------------------------------------------
  Geometry::Geometry() {}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
  Geometry::~Geometry() {}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
  G4VPhysicalVolume* Geometry::Construct()
//------------------------------------------------------------------------------
{
// Get pointer to 'Material Manager'
   G4NistManager* materi_Man = G4NistManager::Instance();

/*
// Get elements from NIST data with symbol, Z ,and NIST material
  G4Element* elAl= G4NistManager::Instance()->FindOrBuildElement("Al");
  G4Element* elSi= G4NistManager::Instance()->FindOrBuildElement("14");
  G4Element* elP= G4NistManager::Instance()->FindOrBuildElement("G4_P");
  //InGaAs G4_GALLIUM_ARSENIDE
  G4Material* materi_Al_1 = materi_Man->FindOrBuildMaterial( "G4_Al" );
  G4Material* materi_Si = materi_Man->FindOrBuildMaterial( "G4_Si" );
  G4Material* materi_Al_2 = materi_Man->FindOrBuildMaterial( "G4_Al" );
*/  
  //silcon density 2.3290 g/cm^3
  G4Element* el_Al; 
  G4Element* el_Si;
  G4Element* el_P; 

  //이렇게 선언하느 이유는 가상클래스라서 선언과 동시에 할당하면 안됌
  //동적 바인딩!!!
  el_Al = new G4Element("Aluminum", "Al", 13,  26.982*g/mole);
  el_Si = new G4Element("Silicon", "Si", 14, 28.086*g/mole);
  el_P = new G4Element("Phosphours", "P", 15, 30.974*g/mole);


  G4Material* N_Semi;
  G4Material* P_Semi;
 
  //Mixture of Materieals 
  N_Semi = new G4Material("N_Semi", 2.71*g/cm3, 2);
  N_Semi->AddElement(el_Si, 99.6*perCent);
  N_Semi->AddElement(el_Al, 0.4*perCent);
  P_Semi = new G4Material("P_Semi", 2.73*g/cm3, 2);
  P_Semi->AddElement(el_Si, 99.6*perCent);
  P_Semi->AddElement(el_P, 0.4*perCent);

// Define 'World Volume'
   // Define the shape of solid
   G4double leng_X_World = 2.0*m;           // X-full-length of world
   G4double leng_Y_World = 2.0*m;           // Y-full-length of world
   G4double leng_Z_World = 2.0*m;           // Z-full-length of world
   auto solid_World =
     new G4Box( "Solid_World", leng_X_World/2.0, leng_Y_World/2.0, leng_Z_World/2.0 );

   // Define logical volume of world
   G4Material* materi_World = materi_Man->FindOrBuildMaterial( "G4_Galactic" ); // geant4 에서 정의한 10 ^-25 g/cm^3 진공, 임계 밀도 10^-31 g/cm^3
   //G4Material* materi_World = materi_Man->FindOrBuildMaterial( "G4_AIR" );
   auto logVol_World = new G4LogicalVolume( solid_World, materi_World, "LogVol_World" );
   logVol_World->SetVisAttributes ( G4VisAttributes::Invisible );

   // Placement of 'World Volume'
   G4int copyNum_World = 0;                 // Set ID number of world
   auto physVol_World  = new G4PVPlacement( G4Transform3D(), "PhysVol_World",
                                           logVol_World, 0, false, copyNum_World/*????*/ );

   // Define the shape of solid 1
   G4double leng_X_World1 = 1.0*cm;           // X-full-length of world
   G4double leng_Y_World1 = 1.0*cm;           // Y-full-length of world
   G4double leng_Z_World1 = 0.4*cm;           // Z-full-length of world
   auto solid_N_Semi_1 =
     new G4Box( "Solid_N_Semi_1", leng_X_World1/2.0, leng_Y_World1/2.0, leng_Z_World1/2.0 );

   G4double pos_X_LogV1 =-1.0*cm;            // X-location LogV
   G4double pos_Y_LogV1 = 1.0*cm;            // Y-location LogV
   G4double pos_Z_LogV1 =-0.12*cm;            // Z-location LogV
   //Define logical volume of solid 1
   //G4Material* materi_BGO = materi_Man->FindOrBuildMaterial( "G4_BGO" );
   G4double pos_X_LogV2 = 0.0*cm;            // X-location LogV
   G4double pos_Y_LogV2 = 0.0*cm;            // Y-location LogV
   G4double pos_Z_LogV2 = 0.0*cm;            // Z-location LogV

   G4double pos_X_LogV3 =+1.0*cm;            // X-location LogV
   G4double pos_Y_LogV3 = 1.0*cm;            // Y-location LogV
   G4double pos_Z_LogV3 =-0.12*cm;            // Z-location LogV

   //Define the shape of solid 2
   G4double leng_X_World2 = 4.0*cm;           // X-full-length of world
   G4double leng_Y_World2 = 4.0*cm;           // Y-full-length of world
   G4double leng_Z_World2 = 2.0*cm;           // Z-full-length of world
   auto solid_P_Semi =
     new G4Box( "Solid_P_Semi", leng_X_World2/2.0, leng_Y_World2/2.0, leng_Z_World2/2.0 );
   //Define logical volume of solid 2
   
   G4VSolid* hollowBox;
   hollowBox = new G4SubtractionSolid("hollow1",solid_P_Semi,solid_N_Semi_1,0,G4ThreeVector(pos_X_LogV1,pos_Y_LogV1,pos_Z_LogV3));

  

   //Define the shape of solid 3
   G4double leng_X_World3 = 1.0*cm;           // X-full-length of world
   G4double leng_Y_World3 = 1.0*cm;           // Y-full-length of world
   G4double leng_Z_World3 = 0.4*cm;           // Z-full-length of world
   auto solid_N_Semi_2 =
     new G4Box( "Solid_N_Semi_2", leng_X_World3/2.0, leng_Y_World3/2.0, leng_Z_World3/2.0 );

   hollowBox = new G4SubtractionSolid("hollow2",hollowBox,solid_N_Semi_2,0,G4ThreeVector(pos_Y_LogV3,pos_Y_LogV3,pos_Z_LogV3));
  
   auto logVol_N_Semi_1 = new G4LogicalVolume( solid_N_Semi_1, N_Semi, "logVol_N_Semi_1",
                                          0, 0, 0 );
   auto logVol_P_Semi = new G4LogicalVolume( hollowBox, P_Semi, "LogVol_P_Semi",
                                          0, 0, 0 );   
   auto logVol_N_Semi_2 = new G4LogicalVolume( solid_N_Semi_2, N_Semi, "logVol_N_Semi_2",
                                          0, 0, 0 );


   // Placement of 'The Detectors' into 'World Volume'
   // Create G4Transform3D to define rotation/translation

   auto threeVect_LogV1 = G4ThreeVector( pos_X_LogV1, pos_Y_LogV1, pos_Z_LogV1 );
   auto rotMtrx_LogV1   = G4RotationMatrix();
   auto trans3D_LogV1   = G4Transform3D( rotMtrx_LogV1, threeVect_LogV1 );

   // Install 1st 'Al Detector'
   G4int copyNum_LogV1 = 1000;               // Set ID number of LogV
   
   // Install 2st 'Si Detector'
   G4int copyNum_LogV2 = 2000;               // Set ID number of LogV
   auto threeVect_LogV2 = G4ThreeVector( pos_X_LogV2, pos_Y_LogV2, pos_Z_LogV2 );
   auto trans3D_LogV2 = G4Transform3D( rotMtrx_LogV1, threeVect_LogV2 );


   // Install 3rd 'Al Detector'
   G4int copyNum_LogV3 = 3000;                     // Set ID number of LogV
   auto threeVect_LogV3 = G4ThreeVector( pos_X_LogV3, pos_Y_LogV3, pos_Z_LogV3 );
   auto trans3D_LogV3 = G4Transform3D( rotMtrx_LogV1, threeVect_LogV3 );



   new G4PVPlacement( trans3D_LogV1 , "PhysVol_N_Semi_1", logVol_N_Semi_1,
                      physVol_World, false ,copyNum_LogV1);
   new G4PVPlacement( trans3D_LogV2 , "PhysVol_P_Semi", logVol_P_Semi,
                      physVol_World, false ,copyNum_LogV2);
   new G4PVPlacement( trans3D_LogV3 , "PhysVol_N_Semi_2", logVol_N_Semi_2,
                      physVol_World, false ,copyNum_LogV3);

// Sensitive volume
    auto aSV = new SensitiveVolume("SensitiveVolume");
    //Add Sensitivity to the logical volume
    logVol_N_Semi_1->SetSensitiveDetector(aSV);         // Add sensitivity to the logical volume
    logVol_P_Semi->SetSensitiveDetector(aSV);
    logVol_N_Semi_2->SetSensitiveDetector(aSV);
    auto SDman = G4SDManager::GetSDMpointer();
    SDman->AddNewDetector(aSV);

// Return the physical volume of 'World'
   return physVol_World;
}
