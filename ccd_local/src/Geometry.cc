//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Geometry.cc
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "Geometry.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include "G4NistManager.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"
#include "SensitiveVolume.hh"
#include "G4SDManager.hh"

#include "G4TransportationManager.hh"
#include "G4IntegrationDriver.hh"
#include "G4ChordFinder.hh"
#include "G4FieldManager.hh"
#include "G4UniformMagField.hh"
#include "G4UniformElectricField.hh"
#include "G4PropagatorInField.hh"


//------------------------------------------------------------------------------
  Geometry::Geometry() {}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
  Geometry::~Geometry() {}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
  G4VPhysicalVolume* Geometry::Construct()
//------------------------------------------------------------------------------
{
// Get pointer to 'Material Manager'
   G4NistManager* materi_Man = G4NistManager::Instance();

  //silcon density 2.3290 g/cm^3
  G4Element* el_Al; 
  G4Element* el_Si;
  G4Element* el_P; 

  el_Al = new G4Element("Aluminum", "Al", 13,  26.982*g/mole);
  el_Si = new G4Element("Silicon", "Si", 14, 28.086*g/mole);
  el_P = new G4Element("Phosphours", "P", 15, 30.974*g/mole);

  G4Material* N_Semi;
  G4Material* P_Semi;
 
  //Mixture of Materieals 
  N_Semi = new G4Material("N_Semi", 0.0271*g/cm3, 2);
  N_Semi->AddElement(el_Si, 99.6*perCent);
  N_Semi->AddElement(el_Al, 0.4*perCent);
  P_Semi = new G4Material("P_Semi", 0.073*g/cm3, 2);
  P_Semi->AddElement(el_Si, 99.6*perCent);
  P_Semi->AddElement(el_P, 0.4*perCent);
  //declear for check implementation of local EM field
  G4Material* vacc = materi_Man->FindOrBuildMaterial( "G4_Galactic" );
  G4Material* air = materi_Man->FindOrBuildMaterial( "G4_AIR" );

// Define 'World Volume'
   // Define the shape of solid
   G4double leng_X_World = 2.0*m;           // X-full-length of world
   G4double leng_Y_World = 2.0*m;           // Y-full-length of world
   G4double leng_Z_World = 2.0*m;           // Z-full-length of world
   auto solid_World =
     new G4Box( "Solid_World", leng_X_World/2.0, leng_Y_World/2.0, leng_Z_World/2.0 );

   // Define logical volume of world
   G4Material* materi_World = materi_Man->FindOrBuildMaterial( "G4_Galactic" ); // geant4 에서 정의한 10 ^-25 g/cm^3 진공, 임계 밀도 10^-31 g/cm^3
   //G4Material* materi_World = materi_Man->FindOrBuildMaterial( "G4_AIR" );
   auto logVol_World = new G4LogicalVolume( solid_World, materi_World, "LogVol_World" );
   logVol_World->SetVisAttributes ( G4VisAttributes::Invisible );

   // Placement of 'World Volume'
   G4int copyNum_World = 0;                 // Set ID number of world
   auto physVol_World  = new G4PVPlacement( G4Transform3D(), "PhysVol_World",
                                           logVol_World, 0, false, copyNum_World/*????*/ );

   // Define the shape of solid 1
   G4double leng_X_World1 = 50.0*cm;           // X-full-length of world
   G4double leng_Y_World1 = 50.0*cm;           // Y-full-length of world
   G4double leng_Z_World1 = 50.0*cm;           // Z-full-length of world
   auto solid_N_Semi =
     new G4Box( "Solid_N_Semi", leng_X_World1/2.0, leng_Y_World1/2.0, leng_Z_World1/2.0 );

   //Define logical volume of solid 1
   //G4Material* materi_BGO = materi_Man->FindOrBuildMaterial( "G4_BGO" );
  
  /* after check implementation of local EM field, delete comment 
  auto logVol_N_Semi = new G4LogicalVolume( solid_N_Semi, N_Semi, "LogVol_N_Semi",
                                          0, 0, 0 );
  */
  
  //after check implementation of local EM field, delete this
      auto logVol_N_Semi = new G4LogicalVolume( solid_N_Semi, air, "LogVol_N_Semi",
                                          0, 0, 0 );

   //Define the shape of solid 2
   G4double leng_X_World2 = 50.0*cm;           // X-full-length of world
   G4double leng_Y_World2 = 50.0*cm;           // Y-full-length of world
   G4double leng_Z_World2 = 50.0*cm;           // Z-full-length of world
   auto solid_P_Semi =
     new G4Box( "Solid_P_Semi", leng_X_World2/2.0, leng_Y_World2/2.0, leng_Z_World2/2.0 );
   //Define logical volume of solid 2
   
   /* after check implementation of local EM field, delete comment 
   auto logVol_P_Semi = new G4LogicalVolume( solid_P_Semi, P_Semi, "LogVol_P_Semi",
                                          0, 0, 0 );
   */
  
  //after check implementation of local EM field, delete this
      auto logVol_P_Semi = new G4LogicalVolume( solid_P_Semi, vacc, "LogVol_P_Semi",
                                          0, 0, 0 );

   // Placement of 'The Detectors' into 'World Volume'
   // Create G4Transform3D to define rotation/translation
   G4double pos_X_LogV1 = 0.0*cm;            // X-location LogV
   G4double pos_Y_LogV1 = 0.0*cm;            // Y-location LogV
   G4double pos_Z_LogV1 =+25.0*cm;            // Z-location LogV
   auto threeVect_LogV1 = G4ThreeVector( pos_X_LogV1, pos_Y_LogV1, pos_Z_LogV1 );
   auto rotMtrx_LogV1   = G4RotationMatrix();
   auto trans3D_LogV1   = G4Transform3D( rotMtrx_LogV1, threeVect_LogV1 );

   G4double pos_X_LogV2 = 0.0*cm;            // X-location LogV
   G4double pos_Y_LogV2 = 0.0*cm;            // Y-location LogV
   G4double pos_Z_LogV2 =-25.0*cm;            // Z-location LogV
   auto threeVect_LogV2 = G4ThreeVector( pos_X_LogV2, pos_Y_LogV2, pos_Z_LogV2 );
   auto rotMtrx_LogV2   = G4RotationMatrix();
   auto trans3D_LogV2   = G4Transform3D( rotMtrx_LogV2, threeVect_LogV2 );

   // Install 1st 'Al Detector'
   G4int copyNum_LogV = 1000;               // Set ID number of LogV
   new G4PVPlacement( trans3D_LogV1 , "PhysVol_N_Semi", logVol_N_Semi,
                      physVol_World, false ,copyNum_LogV);

   // Install 2st 'Si Detector'
   copyNum_LogV = 2000;               // Set ID number of LogV
   new G4PVPlacement( trans3D_LogV2 , "PhysVol_P_Semi", logVol_P_Semi,
                      physVol_World, false ,copyNum_LogV);


// Sensitive volume
    auto aSV = new SensitiveVolume("SensitiveVolume");
    //Add Sensitivity to the logical volume
    logVol_N_Semi->SetSensitiveDetector(aSV);         // Add sensitivity to the logical volume
    logVol_P_Semi->SetSensitiveDetector(aSV);
    auto SDman = G4SDManager::GetSDMpointer();
    SDman->AddNewDetector(aSV);

G4UniformElectricField* pEMfield = new G4UniformElectricField(G4ThreeVector(0.0,100000.0*kilovolt/cm,0.0));
// Create an equation of motion for this field
G4EqMagElectricField* pEquation = new G4EqMagElectricField(pEMfield);
G4int nvar = 8;
// Create the Runge-Kutta 'stepper' using the efficient 'DoPri5' method
auto pStepper = new G4DormandPrince745( pEquation, nvar );
G4FieldManager* localFieldMgr = new G4FieldManager(pEMfield);
//G4FieldManager* localFieldMgr = new G4FieldManager();

//for global field 
//G4FieldManager* localFieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
//Set this field to the global field manager
//localFieldMgr->SetDetectorField( pEMfield );

G4double minStep = 0.001*mm ; // minimal step of 10 microns
// The driver will ensure that integration is control to give acceptable integration error
auto pIntgrationDriver = new G4IntegrationDriver<G4DormandPrince745>(minStep, pStepper,nvar);
G4ChordFinder* pChordFinder = new G4ChordFinder(pIntgrationDriver); 
localFieldMgr->SetChordFinder(pChordFinder);
localFieldMgr->SetDeltaOneStep(1e-3*mm);
localFieldMgr->SetDeltaIntersection(1e-4*mm);

/* //for field propagation
G4PropagationInField* fieldPropagator = G4TransportationManager::GetTransportationManager()->GetPropagationInField();
fieldPropagator -> SetMinimumEpsilonStep(1e-5*mm);
fieldPropagator -> SetMaximuilomEpsilonStep(1e-2*mm);
fieldPropagator ->SetLargestAcceptableStep(10*m);
*/

//for local field
G4bool allLocal = true;
logVol_N_Semi->SetFieldManager(localFieldMgr, allLocal);
//logVol_P_Semi->SetFieldManager(localFieldMgr, allLocal);    
      
// Return the physical volume of 'World'
   return physVol_World;
}
