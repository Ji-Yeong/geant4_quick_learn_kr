//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
#include "FieldMessenger.hh"

#include "ElectricFieldSetup.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FieldMessenger::FieldMessenger(ElectricFieldSetup* fieldSetup)
 : G4UImessenger(),
   fElFieldSetup(fieldSetup),
   fFieldDir(0),
   fStepperCmd(0),
   fElFieldZCmd(0),
   fElFieldCmd(0),
   fMinStepCmd(0),
   fUpdateCmd(0)
{
  fFieldDir = new G4UIdirectory("/field/");
  fFieldDir->SetGuidance("field tracking control.");

  fStepperCmd = new G4UIcmdWithAnInteger("/field/setStepperType",this);
  fStepperCmd->SetGuidance("Select stepper type for electric field");
  fStepperCmd->SetParameterName("choice",true);
  fStepperCmd->SetDefaultValue(4);
  fStepperCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  fUpdateCmd = new G4UIcmdWithoutParameter("/field/update",this);
  fUpdateCmd->SetGuidance("Update calorimeter geometry.");
  fUpdateCmd->SetGuidance("This command MUST be applied before \"beamOn\" ");
  fUpdateCmd->SetGuidance("if you changed geometrical value(s).");
  fUpdateCmd->AvailableForStates(G4State_Idle);

  fElFieldZCmd = new G4UIcmdWithADoubleAndUnit("/field/setFieldZ",this);
  fElFieldZCmd->SetGuidance("Define uniform Electric field.");
  fElFieldZCmd->SetGuidance("Electric field will be in Z direction.");
  fElFieldZCmd->SetGuidance("Value of Electric field has to be given in volt/m");
  fElFieldZCmd->SetParameterName("Ez",false,false);
  fElFieldZCmd->SetDefaultUnit("megavolt/m");
  fElFieldZCmd->AvailableForStates(G4State_Idle);
 
  fElFieldCmd = new G4UIcmdWith3VectorAndUnit("/field/setField",this);
  fElFieldCmd->SetGuidance("Define uniform Electric field.");
  fElFieldCmd->SetGuidance("Value of Electric field has to be given in volt/m");
  fElFieldCmd->SetParameterName("Ex","Ey","Ez",false,false);
  fElFieldCmd->SetDefaultUnit("megavolt/m");
  fElFieldCmd->AvailableForStates(G4State_Idle);
 
  fMinStepCmd = new G4UIcmdWithADoubleAndUnit("/field/setMinStep",this);
  fMinStepCmd->SetGuidance("Define minimal step");
  fMinStepCmd->SetParameterName("min step",false,false);
  fMinStepCmd->SetDefaultUnit("mm");
  fMinStepCmd->AvailableForStates(G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FieldMessenger::~FieldMessenger()
{
  delete fStepperCmd;
  delete fElFieldZCmd;
  delete fElFieldCmd;
  delete fMinStepCmd;
  delete fFieldDir;
  delete fUpdateCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FieldMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if( command == fStepperCmd )
    fElFieldSetup->SetStepperType(fStepperCmd->GetNewIntValue(newValue));
  if( command == fUpdateCmd )
    fElFieldSetup->UpdateIntegrator();
  if( command == fElFieldZCmd )
    fElFieldSetup->SetFieldZValue(fElFieldZCmd->GetNewDoubleValue(newValue));
  if( command == fElFieldCmd )
    fElFieldSetup->SetFieldValue(fElFieldCmd->GetNew3VectorValue(newValue));
  if( command == fMinStepCmd )
    fElFieldSetup->SetMinStep(fMinStepCmd->GetNewDoubleValue(newValue));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
