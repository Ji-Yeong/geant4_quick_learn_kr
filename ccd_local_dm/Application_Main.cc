//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Geant4 Application: Tutorial course for Hep/Medicine Users
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "Geometry.hh"
#include "UserActionInitialization.hh"
#include "PhysicsList.hh"

#include "G4UImanager.hh"
#include "G4UIcommand.hh"
#include "G4RunManager.hh"
#include "G4MTRunManager.hh"
#include "Randomize.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include "FTFP_BERT.hh"
//-------------------------------------------------------------------------------
  int main( int argc, char** argv )
//-------------------------------------------------------------------------------
{
   /*
   // Construct the default run manager
   #ifdef G4MULTITHREADED
   G4MTRunManager* runManager = new G4MTRunManager;
   G4int nThreads = std::min(G4Threading::G4GetNumberOfCores(),2);
   runManager->SetNumberOfThreads(nThreads);
   G4cout << "===== dmparticle is started with " 
         <<  runManager->GetNumberOfThreads() << " threads =====" << G4endl;
   #else
   G4RunManager* runManager = new G4RunManager();
   #endif
   */
   // Construct the default run manager
   auto runManager = new G4RunManager;

   //choose the Random engine
   CLHEP::HepRandom::setTheEngine(new CLHEP::Ranlux64Engine);

// Set up mandatory user initialization: Geometry
   runManager->SetUserInitialization( new Geometry );

// Set up mandatory user initialization: Physics-List
   PhysicsList* phys = new PhysicsList();
   runManager->SetUserInitialization( new FTFP_BERT );
   runManager->SetUserInitialization( phys );
   //runManager->SetUserInitialization( new NuBeam ); //중성미자 상호작용 확인용

// Set up user initialization: User Actions
   runManager->SetUserInitialization( new UserActionInitialization );

// Initialize G4 kernel
   runManager->Initialize();

// Create visualization environment
   auto visManager = new G4VisExecutive;
   visManager->Initialize();

// Start interactive session
   auto uiExec = new G4UIExecutive( argc, argv );
   G4UImanager*  uiManager = G4UImanager::GetUIpointer();
   uiManager->ApplyCommand( "/control/execute GlobalSetup.mac" );
   uiExec->SessionStart();

// Job termination
   delete uiExec;
   delete visManager;
   delete runManager;

   return 0;
}
