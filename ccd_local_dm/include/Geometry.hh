//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Geometry.hh
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifndef Geometry_h
#define Geometry_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4ElectroMagneticField.hh"
#include "G4ElectricField.hh"
#include "G4EqMagElectricField.hh"
#include "G4UniformElectricField.hh"
#include "G4UniformMagField.hh"
#include "G4ThreeVector.hh"
#include "G4DormandPrince745.hh"

class G4FieldManager;
class G4ChordFinder;
class G4ElectroMagneticField;
class G4EquationOfMotion;
class G4Mag_EqRhs;
class G4MagIntegratorStepper;
class G4MagInt_Driver;
class G4EqMagElectricField;
class G4LogicalVolume;
class G4VPhysicalVolume;

//------------------------------------------------------------------------------
  class Geometry : public G4VUserDetectorConstruction
//------------------------------------------------------------------------------
{
  public:
    Geometry();
   ~Geometry();
    void ElectricField();
    G4VPhysicalVolume* Construct();

  private:
  
  //for non uniform field 
  //void MyField::GetFieldValue(const double Point[4], double * filed)const;
  
  //G4ElectricField* pEMfield;
  //G4EqMagElectricField* pEquation;
  //G4ChordFinder* pChordFinder ;
  //G4FieldManager* fieldMgr;
  //G4UniformMagField* magfield;
  //G4LogicalVolume *logVol_N_Semi;
  //G4LogicalVolume *logVol_P_Semi;

  //void localElectroMagneticField::ElectroMagneticField(const double x[3], double B[6]) const ;
  //구현 할려고 선언 부분

};
#endif
