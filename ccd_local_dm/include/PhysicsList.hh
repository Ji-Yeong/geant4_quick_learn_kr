
#ifndef PhysicsList_h
#define PhysicsList_h 1

#include "G4VModularPhysicsList.hh"
#include "globals.hh"
class G4VPhysicsConstructor;
class PhysicsListMessenger;
class StepMax;

class PhysicsList: public G4VModularPhysicsList
{
public:
  PhysicsList();
  virtual ~PhysicsList();
  virtual void ConstructParticle(); // 입자 생성
  virtual void SetCuts(); // 최신 지안트4 버전에서 개인 수정 추천 안함
  virtual void ConstructProcess(); // 물리 과정 생성
  
  void AddPhysicsList(const G4String& name);
  void SetLDMPhotonMass(G4double val);
  void SetLDMHiMass(G4double val);

private:
  void AddDarkMatter(); // 암흑 물질 추가 하는 함수
  G4VPhysicsConstructor*  fEmPhysicsList;
  G4VPhysicsConstructor*  fDecayPhysicsList;
  G4String                fEmName;
  PhysicsListMessenger* fMessenger;
  G4double fLDMPhotonMass; 
  G4double fLDMHiMass;
  G4bool fLDMPhoton;
  G4bool fLDMHi;
};
#endif
